#!/usr/bin/env bash

#== Import script args ==

timezone=$(echo "$1")
project_directory=$(echo "$2")

#== Bash helpers ==

function info {
  echo " "
  echo "--> $1"
  echo " "
}

#== Provision script ==

info "Provision-script user: `whoami`"

info  "Install repositories"
yum install -y wget > /dev/null
wget -q https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
wget -q http://rpms.remirepo.net/enterprise/remi-release-7.rpm
rpm -Uvh remi-release-7.rpm epel-release-latest-7.noarch.rpm
yum install yum-utils  > /dev/null
yum-config-manager --enable remi-php71 > /dev/null
cp ${project_directory}/vagrant/etc/yum.repos.d/* /etc/yum.repos.d/

info  "Install advanced tools"
yum install -y git vim nano mc htop zip unzip > /dev/null

info "Configure timezone"
cp /usr/share/zoneinfo/${timezone} /etc/localtime

info "Configure locale"
localedef  -i ru_RU -f UTF-8 ru_RU.UTF-8

info "Disable SELinux"
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
setenforce 0

info "Update OS software"
yum update > /dev/null

info "Install MySQL"
yum remove -y mariadb-libs > /dev/null
yum install -y mysql-server > /dev/null
systemctl start mysqld
systemctl enable mysqld

info "Install nginx"
yum install -y nginx > /dev/null
systemctl enable nginx

info "Install php"
yum install -y php php-mbstring php-mysqlnd php-pecl-imagick php-zip php-memcached php-soap php-opcache php-pecl-xdebug php-pecl-amqp php-dom php-fpm php-process php-pdo-dblib php-gd > /dev/null
systemctl enable php-fpm

info "Install composer"
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

info "Configure php"
sed -i 's/user = apache/user = vagrant/g' /etc/php-fpm.d/www.conf
sed -i 's/group = apache/group = vagrant/g' /etc/php-fpm.d/www.conf
cp ${project_directory}/vagrant/etc/php.d/* /etc/php.d/
cp ${project_directory}/vagrant/etc/php.ini /etc/
cp ${project_directory}/vagrant/usr/lib64/* /usr/lib64/
cp ${project_directory}/vagrant/usr/lib64/php/modules/* /usr/lib64/php/modules/
systemctl restart php-fpm
echo "Done!"

info "Configure nginx"
cp ${project_directory}/vagrant/etc/nginx/* /etc/nginx/
cp ${project_directory}/vagrant/etc/nginx/conf.d/* /etc/nginx/conf.d/
systemctl restart nginx
echo "Done!"

info "Configure MySQL"
cp ${project_directory}/vagrant/etc/my.cnf /etc/
systemctl restart mysqld
mysql_secret=$(sudo grep 'temporary password' /var/log/mysqld.log | awk '{print $NF}')
mysqladmin -u root --password=${mysql_secret} password root
mysql -uroot -proot -e "GRANT ALL ON *.* TO 'vagrant'@'%' IDENTIFIED BY 'vagrant';"
echo "Done!"

info "Initailize databases for MySQL"
mysql -uvagrant -pvagrant -e "CREATE DATABASE IF NOT EXISTS hello CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci';"
echo "Done!"
